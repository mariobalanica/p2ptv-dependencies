#!/bin/bash
if [ -d "/storage/.xbmc" ]; then
  if [ -d "/storage/.kodi" ]; then
  mcversion=".kodi"
  echo "kodi installed"
  else
  mcversion=".xbmc"
  echo "xbmc installed"
  fi
elif [ -d "/storage/.kodi" ]; then
  mcversion=".kodi"
  echo "kodi installed"
else
  mcversion=".kodi"
  echo "none...assuming kodi"
fi

echo $mcversion

if [ $mcversion = '.xbmc' ]; then
  echo "Detected xbmc -> remove libcrypto"
  if [ -f "/storage/.xbmc/userdata/addon_data/plugin.video.p2ptv/acestream/lib/libcrypto.so.1.0.0" ]; then
      echo "Trying to remove libcrypto"
      rm "/storage/.xbmc/userdata/addon_data/plugin.video.p2ptv/acestream/lib/libcrypto.so.1.0.0"
      echo "libcrypto removed"
  fi
fi

export LD_LIBRARY_PATH=/storage/$mcversion/userdata/addon_data/plugin.video.p2ptv/acestream/lib
/storage/$mcversion/userdata/addon_data/plugin.video.p2ptv/acestream/acestreamengine --lib-path /storage/$mcversion/userdata/addon_data/plugin.video.p2ptv/acestream --client-console $1 $2
